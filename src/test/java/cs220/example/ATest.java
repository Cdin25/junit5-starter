package cs220.example;  // same package as SUT (subject under test)

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ATest {

    @Test
    void canAccess_A_withoutImportBecauseSamePackage() {
        A a = new A();
        assertNotNull(a);
    }

}
